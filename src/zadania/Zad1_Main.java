package zadania;

import java.util.Arrays;
import java.util.Random;

public class Zad1_Main {
    public static void main(String[] args) {
        Random generator = new Random();

        int[] tab = new int[10];

        // a
        for (int i = 0; i < tab.length; i++) {
            tab[i] = generator.nextInt(21) - 10;
        }

        // b
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + ", ");
        }
        System.out.println();

        // b - inny sposób
        System.out.println(Arrays.toString(tab));

        // c
        int min = tab[0], max = tab[0];
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > max) {
                max = tab[i];
            }
            if (tab[i] < min) {
                min = tab[i];
            }
        }

        // d
        double suma = 0;
        for (int i = 0; i < tab.length; i++) {
            suma += tab[i];
        }
        double srednia = suma / tab.length;

        // e
        int wiekszych = 0;
        int mniejszych = 0;

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > srednia) {
                wiekszych++;
            }
            if (tab[i] < srednia) {
                mniejszych++;
            }
        }

        // f
        for (int i = tab.length - 1; i >= 0; i--) {
            System.out.print(tab[i] + ", ");
        }

        // g
        // sortujemy
        Arrays.sort(tab);

        if (tab.length % 2 == 0) {
            // srednia
            // 10 / 2 = 5 (6 indeks)
            //
            double mediana = (tab[tab.length / 2] + tab[tab.length / 2 - 1]) / 2.0;
            System.out.println("Mediana: " + mediana);
        } else {
            // srodkowy
            double mediana = tab[tab.length / 2];
            System.out.println("Mediana: " + mediana);
        }

    }
}
