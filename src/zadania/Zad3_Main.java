package zadania;

import java.util.Scanner;

public class Zad3_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek zakresu:");
        int poczatekZakresu = scanner.nextInt();

        System.out.println("Podaj koniec zakresu:");
        int koniecZakresu = scanner.nextInt();

        if (poczatekZakresu > koniecZakresu) {
            // przedwczesne zakończenie programu
//             return;
            System.exit(1);
        }

        // a)
        System.out.println("Podaj dzielnik:");
        int dzielnik = scanner.nextInt();

        for (int i = poczatekZakresu; i <= koniecZakresu; i++) {
            if (i % dzielnik == 0) {
                System.out.print(i + ", ");
            }
        }
        System.out.println();

        // b)
        System.out.println("Podaj ilosc dzielnikow:");
        int ilosc = scanner.nextInt();

        int[] dzielniki = new int[ilosc];
        for (int i = 0; i < ilosc; i++) {
            System.out.println("Podaj dzielnik " + i + " : ");
            dzielniki[i] = scanner.nextInt();
        }

        for (int i = poczatekZakresu; i <= koniecZakresu; i++) { // iterujemy zakres
            boolean podzielne = true;

            for (int j = 0; j < dzielniki.length; j++) { // iterujemy dzielniki
                boolean podzielnePrzezTenDzielnik = (i % dzielniki[j] == 0);
                podzielne = podzielne && podzielnePrzezTenDzielnik;
                // liczba musi byc podzielna przez wszystkie wczesniejsze dzielniki
                // oraz przez ten obecny dzielnik
            }
            if (podzielne) {
                System.out.print(i + ", ");
            }
        }
        System.out.println();

    }
}
