package zadania;

import java.util.Scanner;

public class Zad8_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String imie = scanner.nextLine();
//        String imie = new String("Pawel");

        // NIE WOLNO PORÓWNYWAĆ STRING PRZEZ ==
        if (imie == "Pawel") {
            System.out.println("OK!");
        } else {
            System.out.println("Nie!");
        }

        // NICEE!
        if (imie.equals("Pawel")) {
            System.out.println("OK!");
        } else {
            System.out.println("Nie!");
        }

        // imie == "Anna" // <- źle!
        switch (imie) {
            case "Anna":
                System.out.println("Imie to: " + imie);
                break;
            case "Jonasz":
                System.out.println("Imie to: " + imie);
                break;
            case "Cezary":
                System.out.println("Imie to: " + imie);
                break;
            default:
                System.out.println("Nie znaleziono");
        }

        if (imie.equals("Anna")) {
            System.out.println("Imie to: " + imie);
        } else if (imie.equals("Jonasz")) {
            System.out.println("Imie to: " + imie);
        } else if (imie.equals("Cezary")) {
            System.out.println("Imie to: " + imie);
        } else {
            System.out.println("Nie znaleziono");
        }

        if (imie.equals("Anna") || imie.equals("Jonasz") || imie.equals("Cezary")) {
            System.out.println("Imie to: " + imie);
        } else {
            System.out.println("Nie znaleziono");
        }


    }
}
