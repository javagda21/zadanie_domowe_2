package zadania;

import java.util.Arrays;
import java.util.Random;

public class Zad2_Main {
    public static void main(String[] args) {
        final int zakres = 10;

        Random generator = new Random();

        int[] tablica = new int[20];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = generator.nextInt(zakres) + 1;
        }
        System.out.println(Arrays.toString(tablica));

        int[] liczniki = new int[zakres + 1];

        // 1 1 2 0 3 3

        // 0, 1, 2, 3 < -indeksy tablicy liczniki
        // 0, 0, 0, 0 < -wartości tablicy liczniki

        for (int i = 0; i < tablica.length; i++) {
            //
//            System.out.println("Liczba : " + tablica[i]);
//            System.out.println("Zwiększam licznik liczby: " + tablica[i]);
            liczniki[tablica[i]]++;
        }

        System.out.println("Zliczone wystąpienia:");
        for (int i = 0; i < liczniki.length; i++) {
            System.out.println(i + " -> " + liczniki[i]);
        }

        // sposób 2
        int[] liczniki2 = new int[zakres + 1];
        for (int i = 0; i < liczniki2.length; i++) { // 0..10

            for (int j = 0; j < tablica.length; j++) { // czy element tablicy == i
                if (tablica[j] == i) {
                    liczniki2[i]++;
                }
            }
        }
    }
}
