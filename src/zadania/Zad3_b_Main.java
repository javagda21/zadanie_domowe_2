package zadania;

import java.util.Scanner;

public class Zad3_b_Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int poczatekZakresu, koniecZakresu;

        do {
            System.out.println("Podaj początek zakresu:");
            poczatekZakresu = scanner.nextInt();

            System.out.println("Podaj koniec zakresu:");
            koniecZakresu = scanner.nextInt();

        } while (poczatekZakresu > koniecZakresu);

    }

}
