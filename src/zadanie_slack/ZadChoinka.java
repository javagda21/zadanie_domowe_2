package zadanie_slack;

import java.util.Scanner;

public class ZadChoinka {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int wielkosc = scanner.nextInt();

        for (int i = 1; i <= wielkosc; i++) { // i mówi ile będzie gwiazdek
            for (int j = 0; j < wielkosc - i; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            for (int j = 0; j < i - 1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = 0; i < wielkosc; i++) {
            for (int j = wielkosc; j > i + 1; j--) {
                System.out.print(" ");
            }
            for (int j = 0; j <= i * 2; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
