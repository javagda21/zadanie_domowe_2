package zadania_pp;

import java.util.Random;
import java.util.Scanner;

public class Zad14_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();

        String odp;
        double iloscPaliwa = 0.0;
        double cena = 4.44;

        do {
            iloscPaliwa += generator.nextDouble() * 10 + 0.1;

            System.out.println("Aktualna ilosc paliwa:" + iloscPaliwa);
            System.out.println("Aktualna kwota do zapłaty to: " + (cena * iloscPaliwa));

            System.out.println("Czy chcesz kontynuować? (tak/nie)");
            odp = scanner.next();

        } while (odp.equals("tak"));

    }
}
