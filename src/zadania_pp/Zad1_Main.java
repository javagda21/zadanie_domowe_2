package zadania_pp;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zad1_Main {
    public static void main(String[] args) {
        final int ilosc = 6;
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();

        int[] cyfryUzytkownika = new int[ilosc];
        for (int i = 0; i < cyfryUzytkownika.length; i++) {
            System.out.println("Podaj liczbę: " + (i + 1));
            cyfryUzytkownika[i] = scanner.nextInt();
        }

        int[] cyfryLosowe = new int[ilosc];
        for (int i = 0; i < cyfryLosowe.length; i++) {
            cyfryLosowe[i] = generator.nextInt(49) + 1;
        }

        System.out.println("Typowane: " + Arrays.toString(cyfryUzytkownika));
        System.out.println("Losowane: " + Arrays.toString(cyfryLosowe));

        int trafione = 0;
        for (int i = 0; i < cyfryUzytkownika.length; i++) {
            for (int j = 0; j < cyfryLosowe.length; j++) {
                if (cyfryUzytkownika[i] == cyfryLosowe[j]) {
                    trafione++;
                }
            }
        }

        System.out.println("Trafiono " + trafione + " z " + ilosc + " liczb.");

    }
}
