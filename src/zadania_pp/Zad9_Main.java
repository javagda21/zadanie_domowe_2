package zadania_pp;

public class Zad9_Main {
    public static void main(String[] args) {
        int[] liczby = new int[]{1, 2, 3, 4};

        for (int i = 0; i < liczby.length; i++) {
            for (int j = 0; j < liczby.length; j++) {
                for (int k = 0; k < liczby.length; k++) {
                    if (liczby[i] != liczby[k] &&
                            liczby[i] != liczby[j] &&
                            liczby[j] != liczby[k]) {
                        System.out.println(liczby[i] + "" + liczby[j] + "" + liczby[k]);
                    }
                }
            }
        }
    }
}
