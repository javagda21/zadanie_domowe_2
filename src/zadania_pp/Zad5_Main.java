package zadania_pp;

import java.util.Scanner;

public class Zad5_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę: ");
        int liczba = scanner.nextInt();

        // zamiana
        String liczbaString = String.valueOf(liczba);

        int suma = 0;
        String[] znaki = liczbaString.split(""); // podzieli po znaku
        for (int i = 0; i < znaki.length; i++) {
            // "5"
            int wartosc = Integer.parseInt(znaki[i]);

            suma += wartosc;
        }

        System.out.println(suma);

//        Integer li = 1;
//        System.out.println(li.toString()); // NullPointerException
//        System.out.println(String.valueOf(li)); // null
    }
}
