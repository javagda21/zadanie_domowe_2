package zadania_pp;

import java.util.Arrays;

public class Zad11_Main {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] tablica2 = new int[]{12, 23, 3, 5, 1, 2};

        int licznikWspolnych = 0;
        for (int i = 0; i < tablica1.length; i++) {
            for (int j = 0; j < tablica2.length; j++) {
                if (tablica1[i] == tablica2[j]) {
                    licznikWspolnych++;
                }
            }
        }

        System.out.println(licznikWspolnych);
        int[] tablica3 = new int[licznikWspolnych];

        licznikWspolnych = 0;
        for (int i = 0; i < tablica1.length; i++) {
            for (int j = 0; j < tablica2.length; j++) {
                if (tablica1[i] == tablica2[j]) {
                    tablica3[licznikWspolnych++] = tablica1[i];
                }
            }
        }
        System.out.println(Arrays.toString(tablica3));
    }
}
