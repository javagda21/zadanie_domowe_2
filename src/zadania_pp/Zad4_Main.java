package zadania_pp;

import java.util.Arrays;
import java.util.Scanner;

public class Zad4_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj ilosc dzielnikow:");
        int iloscDzielnikow = scanner.nextInt();

        int[] dzielniki = new int[iloscDzielnikow];
        for (int i = 0; i < iloscDzielnikow; i++) {
            dzielniki[i] = scanner.nextInt();
        }

        System.out.println("Dzielniki: " + Arrays.toString(dzielniki));


        System.out.println("Podaj liczbe calkowita: ");
        int liczbaCalkowita = scanner.nextInt();
        int licznikPodzielnych = 0;
        for (int i = 1; i < liczbaCalkowita; i++) {
            boolean podzielna = false;

            for (int j = 0; j < dzielniki.length; j++) {
                if (i % dzielniki[j] == 0) {
                    podzielna = true;
                    break;
                }
            }
            if (podzielna) {
                licznikPodzielnych++;
            }
        }

        int[] tablicaDzielnikow = new int[licznikPodzielnych];
        licznikPodzielnych = 0;
        for (int i = 1; i < liczbaCalkowita; i++) {
            boolean podzielna = false;

            for (int j = 0; j < dzielniki.length; j++) {
                if (i % dzielniki[j] == 0) {
                    podzielna = true;
                    break;
                }
            }
            if (podzielna) {
                tablicaDzielnikow[licznikPodzielnych++] = i;
            }
        }
        System.out.println("Dzielniki: " + Arrays.toString(tablicaDzielnikow));
    }
}
