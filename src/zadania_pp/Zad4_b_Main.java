package zadania_pp;

import java.util.Arrays;
import java.util.Scanner;

public class Zad4_b_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj ilosc dzielnikow:");
        int iloscDzielnikow = scanner.nextInt();

        int[] dzielniki = new int[iloscDzielnikow];
        for (int i = 0; i < iloscDzielnikow; i++) {
            dzielniki[i] = scanner.nextInt();
        }

        System.out.println("Dzielniki: " + Arrays.toString(dzielniki));


        System.out.println("Podaj liczbe calkowita: ");
        int liczbaCalkowita = scanner.nextInt();
        for (int j = 0; j < dzielniki.length; j++) {
            int licznikPodzielnych = 0;
            // zliczenie dzielników
            for (int i = 1; i < liczbaCalkowita; i++) {
                if (i % dzielniki[j] == 0) {
                    // dzielnik
                    licznikPodzielnych++;
                }
            }
            int[] tablicaDzielnikow = new int[licznikPodzielnych];
            licznikPodzielnych = 0;
            for (int i = 1; i < liczbaCalkowita; i++) {
                if (i % dzielniki[j] == 0) {
                    // dzielnik
                    tablicaDzielnikow[licznikPodzielnych++] = i;
                }
            }
            System.out.println("Podzielnych przez " + dzielniki[j] + " -> " + Arrays.toString(tablicaDzielnikow));
        }


    }
}
