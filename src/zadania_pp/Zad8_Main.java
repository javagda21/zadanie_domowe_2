package zadania_pp;

import java.util.Arrays;
import java.util.Comparator;

public class Zad8_Main {
    public static void main(String[] args) {
//        Integer[] tablica = new Integer[]{8, 7, 6, 5, 4, 3, 2, 1};
        Integer[] tablica = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8};
        Arrays.sort(tablica, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1 > o2){
                    return -1;
                }else if (o1 < o2){
                    return 1;
                }
                return 0;
            }
        });

        System.out.println(Arrays.toString(tablica));
    }
}
