package zadania_pp;

import java.util.Arrays;
import java.util.Scanner;

public class Zad10_Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linie:");
        String linia = scanner.nextLine();

        String[] tablicaStringow = linia.split(",");
        int[] tablicaIntow = new int[tablicaStringow.length];
        for (int i = 0; i < tablicaStringow.length; i++) {
            tablicaIntow[i] = Integer.parseInt(tablicaStringow[i]);
        }

        int licznikUnikatowych = 0;
        for (int i = 0; i < tablicaIntow.length; i++) {
            boolean czyWystapil = false;
            // pętla sprawdzająca
            for (int j = 0; j < i; j++) {
                if (tablicaIntow[i] == tablicaIntow[j]) {
                    czyWystapil = true;
                    break;
                }
            }
            if (!czyWystapil) {
                licznikUnikatowych++;
            }
        }

        int[] tablicaUnikatowa = new int[licznikUnikatowych];
        licznikUnikatowych = 0;
        for (int i = 0; i < tablicaIntow.length; i++) {

            boolean czyWystapil = false;
            // pętla sprawdzająca
            for (int j = 0; j < i; j++) {
                if (tablicaIntow[i] == tablicaIntow[j]) {
                    czyWystapil = true;
                    break;
                }
            }
            if (!czyWystapil) {
                tablicaUnikatowa[licznikUnikatowych++] = tablicaIntow[i];
            }
        }

        System.out.println(Arrays.toString(tablicaUnikatowa));
    }
}
